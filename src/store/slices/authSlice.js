import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  username: null,
  email: null,
  userId: null,
  isSignedIn: false,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    signIn: (state, { payload }) => {
      state.email = payload.email;
      state.userId = payload.userId;
      state.username = payload.username;
      state.isSignedIn = payload.isSignedIn;
    },
    signOut: (state) => initialState,
  },
});

//actions
export const { signIn, signOut } = authSlice.actions;

//reducer
export default authSlice.reducer;
