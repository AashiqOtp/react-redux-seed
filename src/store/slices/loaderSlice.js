import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  show: false,
};

export const loaderSlice = createSlice({
  name: "loader",
  initialState,
  reducers: {
    showLoader: (state) => {
      state.show = true;
    },
    hideLoader: (state) => {
      state.show = false;
    },
  },
});

//actions
export const { showLoader, hideLoader } = loaderSlice.actions;

//reducer
export default loaderSlice.reducer;
