import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./slices/authSlice";
import loaderSlice from "./slices/loaderSlice";

export const Store = configureStore({
  reducer: {
    auth: authSlice,
    loader: loaderSlice,
  },
});
