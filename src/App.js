import Routes from "./routes";
import "./assets/styles/tailwind.css";
import Loader from "./components/common/loader";
import { useSelector } from "react-redux";

const App = () => {
  const { show } = useSelector((state) => state.loader);
  console.log(show);

  return (
    <>
      {show && <Loader />}
      <div
        className={
          show ? "backdrop z-10" : "absolute bg-gray-200 h-full w-full"
        }
      >
        <Routes />
      </div>
    </>
  );
};

export default App;
