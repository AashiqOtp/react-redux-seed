import React from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import LoginPage from "../components/authentication/login";
import PublicRoute from "./publicRoute";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { clientRoutes } from "../global/clientRoutes";
const Routes = () => {
  return (
    <div className="bg-gray-100">
      <ToastContainer />
      <Router>
        <Switch>
          <PublicRoute path={clientRoutes.LOGIN} exact component={LoginPage} />
        </Switch>
      </Router>
    </div>
  );
};

export default Routes;
