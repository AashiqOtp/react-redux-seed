import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route } from "react-router-dom";
import { clientRoutes } from "../global/clientRoutes";

const PublicRoute = ({ component: Component, ...rest }) => {
  const { isSignedIn } = useSelector((state) => state.auth);

  return (
    <>
      <Route
        {...rest}
        render={(props) => {
          return !isSignedIn ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: clientRoutes.HOME,
                state: { from: props.location.pathname },
              }}
            />
          );
        }}
      />
    </>
  );
};

export default PublicRoute;
